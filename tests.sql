set SERVEROUTPUT on;
execute DBMS_OUTPUT.PUT_LINE('new user_id: ' || C1_USER_T_API.INSERT_NEW('qwe@ex.com', 'qwe', 'qwe'));
execute DBMS_OUTPUT.PUT_LINE('new user_id: ' || C1_USER_T_API.INSERT_NEW('abc@ex.com', 'abc', 'abc'));
execute DBMS_OUTPUT.PUT_LINE('new user_id: ' || C1_USER_T_API.INSERT_NEW('asd@ex.com', 'asd', 'asd'));


execute DBMS_OUTPUT.PUT_LINE('user_id (update name): ' || C1_USER_T_API.UPDATE_USERNAME(1, 'mnb'));
execute DBMS_OUTPUT.PUT_LINE('user_id (update email): ' || C1_USER_T_API.UPDATE_EMAIL(1, 'mnb@ex.com'));
execute DBMS_OUTPUT.PUT_LINE('user_id (update pasword): ' || C1_USER_T_API.UPDATE_PASSWORD(1, 'mnb'));
execute DBMS_OUTPUT.PUT_LINE('user_id (update level): ' || C1_USER_T_API.UPDATE_LEVEL(1, 2));

execute C1_USER_T_API.READ_USER_BY_ID(1);

execute C1_USER_T_API.DELETE_USER(2);