#!/bin/bash

cd sql_parts
echo "" > ../all.sql
for file in level.sql user.sql achievement.sql user_achievement.sql dictionatory.sql sentence.sql sentence_dictionatory.sql model.sql game_type.sql text_properties_type.sql text.sql game.sql  participant.sql triggers.sql; do
	echo -e "/* $file */" >> ../all.sql 
	cat $file >> ../all.sql
	echo -e "\n\n\n\n" >> ../all.sql
done