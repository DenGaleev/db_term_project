CREATE TABLE C1_GAME 
(
  ID NUMBER NOT NULL,
  START_TIME DATE NOT NULL,
  TEXT_ID NUMBER NOT NULL,
  GAME_TYPE_ID NUMBER NOT NULL,
  CONSTRAINT C1_GAME_PK PRIMARY KEY (ID)
  ENABLE 
);
/

ALTER TABLE C1_GAME
ADD CONSTRAINT C1_GAME_FK1 FOREIGN KEY (TEXT_ID)
REFERENCES C1_TEXT (ID)
ENABLE;
/

ALTER TABLE C1_GAME
ADD CONSTRAINT C1_GAME_FK2 FOREIGN KEY (GAME_TYPE_ID)
REFERENCES C1_GAME_TYPE (ID)
ENABLE;
/