create or replace package C1_USER_T_API
is

-- insert
function insert_new (
  p_EMAIL    in C1_USER.EMAIL%type,
  p_USERNAME in C1_USER.USERNAME%type,
  p_PASSWORD in C1_USER.PASSWORD%type
) return C1_USER.ID%type;


function update_username (
  p_ID in C1_USER.ID%type,
  p_USERNAME in C1_USER.USERNAME%type
) return C1_USER.ID%type;

function update_email (
  p_ID in C1_USER.ID%type,
  p_EMAIL in C1_USER.EMAIL%type
) return C1_USER.ID%type;

function update_password (
  p_ID in C1_USER.ID%type,
  p_PASSWORD in C1_USER.PASSWORD%type
) return C1_USER.ID%type;

function update_experience (
  p_ID in C1_USER.ID%type,
  p_EXPERIENCE in C1_USER.EXPERIENCE%type
) return C1_USER.ID%type;

function update_level (
  p_ID in C1_USER.ID%type,
  p_LEVEL_ID in C1_USER.LEVEL_ID%type
) return C1_USER.ID%type;

-- read
procedure read_user_by_id (
  p_ID in C1_USER.ID%type
);

-- delete
procedure delete_user (
  p_ID in C1_USER.ID%type
);

end C1_USER_T_API;
/


create or replace package body C1_USER_T_API
is
--insert 
function insert_new (
  p_EMAIL    in C1_USER.EMAIL%type,
  p_USERNAME in C1_USER.USERNAME%type,
  p_PASSWORD in C1_USER.PASSWORD%type
) return C1_USER.ID%type is
    p_LEVEL_ID C1_USER.LEVEL_ID%type;
    result C1_USER.ID%type;
  begin
    select ID into p_LEVEL_ID 
      from (select ID, THRESHOLD from C1_LEVEL order by THRESHOLD)
      where rownum=1;

    insert into C1_USER(PASSWORD, USERNAME, EMAIL, LEVEL_ID) 
      values (p_PASSWORD, p_USERNAME, p_EMAIL, p_LEVEL_ID) 
      returning ID into result;
    return result;
  exception
    when NO_DATA_FOUND then
    return NULL;
  end;

-- update
function update_username (
  p_ID in C1_USER.ID%type,
  p_USERNAME in C1_USER.USERNAME%type
) return C1_USER.ID%type is
    result C1_USER.ID%type;
  begin
    update C1_USER 
      set USERNAME=p_USERNAME 
      where ID=p_ID 
      returning ID into result;
    return result;
  exception
    when NO_DATA_FOUND then
    return NULL;
  end;

function update_email (
  p_ID in C1_USER.ID%type,
  p_EMAIL in C1_USER.EMAIL%type
) return C1_USER.ID%type is
    result C1_USER.ID%type;
  begin
    update C1_USER 
      set EMAIL=p_EMAIL 
      where ID=p_ID 
      returning ID into result;
    return result;
  exception
    when NO_DATA_FOUND then
    return NULL;
  end;

function update_password (
  p_ID in C1_USER.ID%type,
  p_PASSWORD in C1_USER.PASSWORD%type
) return C1_USER.ID%type is
    result C1_USER.ID%type;
  begin
    update C1_USER 
      set PASSWORD=p_PASSWORD 
      where ID=p_ID 
      returning ID into result;
    return result;
  exception
    when NO_DATA_FOUND then
    return NULL;
  end;

function update_experience (
  p_ID in C1_USER.ID%type,
  p_EXPERIENCE in C1_USER.EXPERIENCE%type
) return C1_USER.ID%type is
    result C1_USER.ID%type;
  begin
    update C1_USER 
      set EXPERIENCE=p_EXPERIENCE 
      where ID=p_ID 
      returning ID into result;
    return result;
  exception
    when NO_DATA_FOUND then
    return NULL;
  end;

function update_level (
  p_ID in C1_USER.ID%type,
  p_LEVEL_ID in C1_USER.LEVEL_ID%type
) return C1_USER.ID%type is
    p_EXPERIENCE C1_USER.EXPERIENCE%type;
    t_EXPERIENCE C1_USER.EXPERIENCE%type;
    result C1_USER.ID%type;
  begin
    select EXPERIENCE into p_EXPERIENCE
      from C1_USER
      where ID=p_ID;
    select THRESHOLD into t_EXPERIENCE
      from C1_LEVEL
      where ID=p_LEVEL_ID;
    
    if p_EXPERIENCE<t_EXPERIENCE then
      p_EXPERIENCE:=t_EXPERIENCE; 
    end if;

    update C1_USER
      set LEVEL_ID=p_LEVEL_ID,
          EXPERIENCE=p_EXPERIENCE
      where ID=p_ID
      returning ID into result;
    return result;
  exception
    when NO_DATA_FOUND then
    return NULL;
  end;



--read
procedure read_user_by_id (
  p_ID in C1_USER.ID%type
) as
    name C1_USER.USERNAME%type;
  begin
    select USERNAME into name 
      from C1_USER
      where ID=p_ID;
    
    DBMS_OUTPUT.PUT_LINE('Read user by ID: ' || name);
  exception
    when NO_DATA_FOUND then
    DBMS_OUTPUT.PUT_LINE('Read user by ID: Not found');
  end;



-- delete
procedure delete_user (
  p_ID in C1_USER.ID%type
) as 
  begin
    delete from C1_USER
      where id=p_ID;
  end;

end C1_USER_T_API;
/ 
